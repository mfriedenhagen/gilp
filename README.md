# GitLab Proxy (GiLP)

A proxy for GitLab pipelines which accepts a `$CI_JOB_JWT` and uses a personal access token (PAT) on behalf of the original requester to increase permissions without exposing the PAT

```
 _________________                     ________              ________
|                 |                   |        |            |        |
| GitLab pipeline | ---CI_JOB_JWT---> |  GiLP  | ---PAT---> | GitLab |  
|_________________|                   |________|            |________|
```

## Rationale

GiLP overcomes some restrictions of the GitLab permission model. 
You can use GiLP to enhance permissions if the `$CI_JOB_TOKEN` is too restrictive, e.g., pushing to a repo, making an API call, etc.
This approach is better than defining a top-level PAT as a pipeline variable because the PAT is not exposed to projects and remains in GiLP

See also:

* https://gitlab.com/groups/gitlab-org/-/epics/3559
* https://gitlab.com/gitlab-org/gitlab/-/issues/22367
* https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
* https://docs.gitlab.com/ee/api/README.html#gitlab-ci-job-token
* https://docs.gitlab.com/ee/user/project/new_ci_build_permissions_model.html#dependent-repositories

## Usage

Spin up an instance of GiLP with a PAT with elevated permissions and use `https://:${CI_JOB_JWT}@<gilp-address>/...` instead of `https://gitlab.com/...` in your GitLab pipeline.

### Examples

```shell
# Use GiLP for Git operations on the own repo:
git remote set-url origin https://:${CI_JOB_JWT}@<gilp-address>/${CI_PROJECT_PATH}.git
(...)
# Make an API call:
curl -X POST -H "PRIVATE-TOKEN: ${CI_JOB_JWT}" "https://<gilp-address>/api/v4/projects/${CI_PROJECT_ID}/repository/tags?&tag_name=v1.0.${CI_PIPELINE_IID}&ref=master"
```

### Example pipeline

You can study the following example pipeline: <https://gitlab.com/johanngyger/gilp-test/-/jobs/1013639076>

## Configuration

GiLP supports the following environment variables (see [config.go](internal/gilp/config/config.go)):

* `GILP_TOKEN`: The personal access token (PAT) which GiLP is using to forward requests to gitlab.com
* `GILP_PORT`: The listen port for GiLP (default `8080`)
* `GILP_DUMP_REQUESTS`: Dump all requests, useful for debugging (default `false`)

## Runtime

GiLP is packaged as a Docker image, see [Dockerfile](Dockerfile):

Build locally:

```shell
docker build -t gilp .
docker run -p 8080:8080 -e GILP_TOKEN=<pat> gilp
```

Run with image from the registry:

```shell
docker run -p 8080:8080 -e GILP_TOKEN=<pat> registry.gitlab.com/johanngyger/gilp
```

## Deployment

**TODO:** Provide example Terraform configs to spin up a GiLP instance in the cloud

## Development

This is a pretty standard Go project. The following commands work out of the box:

```shell
go run main.go
go build
go test -v ./...
goimports -w .
golangci-lint run
go get -u -t ./...
go mod tidy
```

